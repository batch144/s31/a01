
const Task = require("./../models/Task");

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then( result => result)
}


module.exports.getById = (params) => {

	return Task.findById(params).then(result => result)
}


module.exports.updateTask = (name) => {
	console.log(name)
	let updateStatus = {
		status: "Complete"
	}
	return Task.findOneAndUpdate({name: name}, updateStatus, {new: "Complete"}).then(result => result)
}