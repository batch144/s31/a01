
//import express module
const express = require("express");

//
const mongoose = require("mongoose");

// express server
const app = express();

//local port and available port
const port = process.env.port || 4000;



//after exporting router module, import it to index.js file
const userRoutes = require("./routes/userRoutes");

const taskRoutes = require("./routes/taskRoutes");


//mongodb connection
mongoose.connect('mongodb+srv://admin:admin123@batch139.idzre.mongodb.net/s31?retryWrites=true&w=majority',
	{useNewUrlParser: true, useUnifiedTopology: true});

//mongodb notification
const db = mongoose.connection;
db.on('error', () => console.error('Connection failed!'));
db.once('open', () => console.log(`Connected to database`));

//middleware

//helps express to understand json payloads
app.use(express.json())

//helps express to understand json payloads
app.use(express.urlencoded({extended: true}))


//routes
	//Root url
		//http://localhost:4000/api/users
		//http://localhost:4000/api/tasks

//middleware that passes all rquest to user routes module
app.use("/api/users", userRoutes)

app.use("/api/tasks", taskRoutes);



app.listen(port, () => console.log(`Server running at port ${port}`));