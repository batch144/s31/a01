
//in order to use mongoose properties and methods, import mongoose modules
const mongoose = require("mongoose");

//Schema
const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "Firstname is required"]
		},
		lastName: {
			type: String,
			required: [true, "Lastname is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNo: {
			type: String,
			required: [true, "Number is required"]
		}
	}
);

//model
	//we make model out of schema because model has methods that can manipulate a query database
module.exports = mongoose.model("User", userSchema);
